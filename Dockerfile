FROM centos:centos7

# install main packages:
RUN yum -y update; yum clean all
RUN yum -y install epel-release s3fs-fuse; yum clean all
RUN yum -y install s3fs-fuse; yum clean all
COPY run.sh /run.sh
ENTRYPOINT exec /run.sh
